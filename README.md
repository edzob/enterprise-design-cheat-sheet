# Enterprise Design Cheat Sheet

The goal of this Enterprise Design Cheat Sheet is 
to be a comprehensive summary of steps and concepts 
needed to create a company from scratch or 
for an existing company to kickstart an essential transformation. 


## Open Knowledge
This document will continuously be updated,
and is available for everyone to see and participate,
since I believe in open science, open access and open knowledge,
and it is a summary of available knowledge that is fast and infinite.

### Initial version
This document is (initially) created by me ([@edzob](https://www.edzob.com)) and 
published under the creative commons BY-SA 4.0 license. 
The content is a summary of available and open knowledge. Feedback is welcome. 

The source of this document is on GitLab as an open repository.
https://gitlab.com/edzob/enterprise-design-cheat-sheet

## PDF
* (v1.2) [Enterprise Design Cheatsheet - 20220120.pdf](https://doi.org/10.5281/zenodo.5884550)
* (v1.1) [Enterprise Design Cheatsheet - 20220114.pdf](https://doi.org/10.5281/zenodo.5850337)
* (v1.0) [Enterprise Design Cheatsheet - 20220113.pdf](https://doi.org/10.5281/zenodo.5844459)

## Enjoy

I hope you enjoy the content of this document and provide some inspiration to investigate one or more aspects of your transformation. You are welcome to contact me to discuss the content and reflect on it. 
Best to you. 


---

## Run
To compile Latex continuous: 

```
$ make clean render LATEXMK_OPTIONS_EXTRA=-pvc 
```

To refresh the PDF continuous:
```
$  evince paper/latexmk/main.pdf
```


This runs the PDF viewer [Evince](https://wiki.gnome.org/Apps/Evince) 
that refreshes automatically when the pdf is changed. 

This is using 
[Docker](https://docs.docker.com/get-docker/) 
and 
[GNU make](https://www.gnu.org/software/make/)
together with 
[latexMK](https://ctan.org/pkg/latexmk?lang=en) 
in a the 
[texlive:latest container](https://hub.docker.com/r/texlive/texlive).

The [texlive:latest container](https://hub.docker.com/r/texlive/texlive).
is updated weekly by the texlive organisation.



---

## Acknowledgment

Thanks to [Martin Isaksson](https://cv.martisak.se/) 
with his great manual 
[How to annoy your co-authors: a gitlab CI Pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/) 
and for his [code-base](https://gitlab.com/martisak/latex-pipeline), that I [adjusted](https://gitlab.com/edzob/latex-pipeline/-/tree/texlive).


Thank you to [makeareadme.com](https://www.makeareadme.com) for the template for this Readme.

---

## Support to you
You can leave questions, remarks etc in the gitlab tracker, 
or just drop me ([Edzo](https://www.edzob.com/page/contact/)) a line.

## Roadmap
1. v Migration from Google Docs to Gitlab
    1. v add the images to the gitlab repo
    1. v fix the layout of the table of contents (TOC)
1. v integrating docker for local build as gitlab cloud build
1. Check the visuals tips from [makeareadme.com](https://www.makeareadme.com)
1. fix every section/chapter on new page
1. crop images to remove captions
1. add caption to images with reference from footnote
1. all images should be transparant png

### Visuals
Depending on what you are making, it can be a good idea to include screenshots 
or even a video (you'll frequently see GIFs rather than actual videos). 
Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Contributing
* Feel free to add or change content.
* Feel free to add comments via the tracker.

## LaTeX
The source of this document is/will be in LaTeX, so that everybody can add content in plain text,
and pdf's can be created in every way.

LaTeX has been around since ever, so there are many many resources out there how to start.
There is for example a coursera course on latex.
Many universities, all over the world, offer their 1-year students a course in LaTeX.

Still not certain where to start. Just drop me ([Edzo](https://www.edzob.com/page/contact/)) a line.


## Version
Edzo will add periodical a new version on Zenodo and edit the readme with the new link.

## Authors
* Edzo Botjes

## License
This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[cc-by-sa]:https://creativecommons.org/licenses/by-sa/4.0/

You are free to:
* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material for any purpose, even commercially.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

This license is acceptable for Free Cultural Works.
The licensor cannot revoke these freedoms as long as you follow the license terms.




